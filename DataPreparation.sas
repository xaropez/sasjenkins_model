cas auto authinfo="/home/sas/.authinfo" host='sasviyahost.ec2.internal' port=5570 sessopts=(caslib=casuser timeout=1800 locale="en_US");
caslib _ALL_ assign;

proc cas;
/* Checar se a tabela existe */
 table.tableexists result=r /
 caslib='PUBLIC'
 name='HMEQ_TRATADA';
 run;
	if (r.exists) then do;
			table.dropTable result=r
			caslib="PUBLIC"
			name="HMEQ_TRATADA";
	end;
quit;

%put &x;
*Adiciono +1 em year off job;
data public.HMEQ_TRATADA;
set public.HMEQ;
YOJ = YOJ +1;
		%if "&x" = "1" %then %do;
			%let debug=cancel;
		%end;
		%else %do;
			%let debug=;
		%end;
run &debug.;

%put &x;
%put &debug;

/*
Value(syserr)	Description
0	Execution completed successfully and without warning messages.
1	Execution was canceled by a user with a RUN CANCEL statement.
2	Execution was canceled by a user with an ATTN or BREAK command.
3	An error in a program run in batch or non-interactive mode caused SAS to enter syntax-check mode.
4	Execution completed successfully but with warning messages.
5	Execution was canceled by a user with an ABORT CANCEL statement.
6	Execution was canceled by a user with an ABORT CANCEL FILE statement.
>6	An error occurred. The value returned is procedure-dependent.
*/
%macro macroutil;
   %if &syserr = 0 or &syserr = 4 %then %do;
		/* Subir tabela para mem�ria*/
		proc casutil
		 incaslib="PUBLIC" outcaslib="PUBLIC";                         
				  			promote casdata="HMEQ_TRATADA";
		run;
   %end;
%mend macroutil;

%macroutil;
%put &syserr;




