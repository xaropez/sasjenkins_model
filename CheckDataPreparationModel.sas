cas auto authinfo="/home/sas/.authinfo" host='sasviyahost.ec2.internal' port=5570 sessopts=(caslib=casuser timeout=1800 locale="en_US");
caslib _ALL_ assign;

proc cas;
	/*Checo se a tabela existe*/
	%if %sysfunc(exist(PUBLIC.HMEQ_TRATADA)) %then %do;
			%let x=0;
	%end;
	%else %do;
		%let x=1;
	%end;
quit;

%put &x;

%macro macroutil;
   %if &x = 1 %then %do;
		/* Exexuto código para dar erro*/
		proc casutil
		 incaslib="PUBLIC" outcaslib="PUBLIC";                         
				  			promote casdata="HMEQ_TRATADA";
		run;
   %end;
%mend macroutil;

%macroutil;