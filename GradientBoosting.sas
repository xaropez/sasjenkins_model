cas auto authinfo="/home/sas/.authinfo" host='sasviyahost.ec2.internal' port=5570 sessopts=(caslib=casuser timeout=1800 locale="en_US");
caslib _ALL_ assign;

/*Criando um modelo de gradient booting para validação*/
proc gradboost data=PUBLIC.HMEQ_TRATADA;
partition fraction(validate=0.3);
target BAD / level=nominal;
input LOAN MORTDUE DEBTINC VALUE YOJ DEROG DELINQ CLAGE NINQ CLNO /
level=interval;
input REASON JOB / level=nominal;
score out=public.hmeq_scored copyvars=(_all_);
savestate rstore=public.astore_gb;
id _all_;
run;

/*Fazendo download do astore, caslib -> file*/
proc astore;
    download rstore=public.astore_gb
             store="/tmp/astore_gb";
quit;

/*Scorando com Astore*/
/*proc astore;
score data=public.HMEQ_TRATADA out=public.HMEQ_SCORED_ASTORE
rstore=public.astore_gb;
quit;*/

/*Proc upload com astore, file -> caslib*/
/*proc astore;
    upload rstore=public.astore_gb_TESTE
             store="/tmp/astore_gb";
quit;*/
